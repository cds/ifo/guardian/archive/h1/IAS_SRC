#   IAS_SRC.py
#
# $Id: IAS_SRC.py 9158 2014-11-18 23:27:56Z alexan.staley@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/asc/h1/guardian/IAS_SRC.py $
#

from ISC_GEN_STATES import \
			gen_REFL_WFS_CENTERING,\
			gen_CORNER_WFS_CENTERING,\
			gen_OFFLOAD_ALIGNMENT

from ISC_DOF import\
			DOWN,\
			gen_LOCK_DOF,\
			SRM_ALIGN


# LOCK_SRY
##################################################
LOCK_SRY = gen_LOCK_DOF()
LOCK_SRY.dof = 'SRY'

# OFFLOAD_SRM_ALIGNMENT
##################################################
OFFLOAD_SRM_ALIGNMENT = gen_OFFLOAD_ALIGNMENT('SRY')
OFFLOAD_SRM_ALIGNMENT.optics = ['SRM']
OFFLOAD_SRM_ALIGNMENT.loops = ['SRC1_P', 'SRC1_Y']

# REFL_WFS_CENTERING_SRY
##################################################
REFL_WFS_CENTERING_SRY = gen_REFL_WFS_CENTERING('SRY')

##################################################

edges = [
# SRY edges:
    ('DOWN', 'LOCK_SRY'),
    ('LOCK_SRY', 'REFL_WFS_CENTERING_SRY'), 
    ('REFL_WFS_CENTERING_SRY','SRM_ALIGN'),
    ('SRM_ALIGN', 'OFFLOAD_SRM_ALIGNMENT'),
]
