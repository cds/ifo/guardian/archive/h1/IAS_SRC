# 2000 is for 1W input.  It's scaled where it's used in the code
#
# $Id$
# $HeadURL$
###########################
## general stuff
###########################
bs_oscillation_thresh = 300 # urads
laser_power = 5 # 10 W


mich_acquire_FMs = ['FM4', 'FM7', 'FM5']

prcl_acquire_FMs = ['FM4', 'FM9', 'FM10']
prm_m2_FMs = ['FM1', 'FM3', 'FM4', 'FM6', 'FM10']
prm_M2_cross_over = 4.5  # in Hz

srcl_acquire_FMs = ['FM9', 'FM10']
srm_m2_FMs = ['FM1', 'FM3', 'FM4', 'FM6', 'FM10']
srm_M2_cross_over_start = 1.0
srm_M2_cross_over = 4.5  # in Hz

###########################
## drmi
###########################
drmi_locked_threshold_pop18I = 45 #2000

drmi_mich_gain_no_arms = 3
drmi_prcl_gain_no_arms = 15
drmi_srcl_gain_no_arms = -50

drmi_mich_gain_als = 2.9#4.0  # was 3
drmi_prcl_gain_als = 8 # was 11.0 until Feb 5 2015
drmi_srcl_gain_als = -45

mich_input_mtrx = 4
prcl_input_mtrx = 3.5
srcl_input_mtrx = 4

drmi_modehop_offset = -800

# FM triggers
drmi_mich_fm_trig_index = [3]
drmi_prcl_fm_trig_index = [2, 3]
drmi_srcl_fm_trig_index = [1, 4]

drmi_srcl_trig_upper_threshold_no_arms = 1.1 #POPAIR_A_RF90
drmi_srcl_trig_lower_threshold_no_arms = 0.7 #POPAIR_A_RF90
drmi_srcl_trig_upper_threshold_als = 2.2 #POPAIR_A_RF90
drmi_srcl_trig_lower_threshold_als = 1.4 #POPAIR_A_RF90
drmi_mich_trig_upper_threshold_als = 20 # was ten before 10 #POPAIR_A_RF18
drmi_mich_trig_lower_threshold_als = 3 #POPAIR_A_RF18
drmi_mich_trig_upper_threshold_no_arms = 5 #POPAIR_A_RF18
drmi_mich_trig_lower_threshold_no_arms = 2 #POPAIR_A_RF18
drmi_prcl_trig_upper_threshold = -100.0 #POPAIR_A_RF18
drmi_prcl_trig_lower_threshold = -100.0 #POPAIR_A_RF18
drmi_srcl_fm_trig_upper_threshold_no_arms = 5.5
drmi_srcl_fm_trig_lower_threshold_no_arms = 1.0
drmi_srcl_fm_trig_upper_threshold_als = 5.5
drmi_srcl_fm_trig_lower_threshold_als = 1.0
drmi_prcl_fm_trig_upper_threshold = 3.5
drmi_prcl_fm_trig_lower_threshold = 1.0
drmi_mich_fm_trig_upper_threshold = 25.0 # 4.5
drmi_mich_fm_trig_lower_threshold = 3.0 # 1.0

###########################
## DRMI 3F
###########################
#refl_27_I_gain = 1.0
#refl_135_Q_gain = 0.7 # this is good for 10 W.
#refl_135_Q_gain = 2.0 # this is good for 3 W
#refl_135_I_gain = 5.0 # 3W
#refl_135_I_gain = 0.45 # 10W

# after BBPF replacement
refl_27_I_gain =  -4.0 #-6.0 #-7.29
refl_135_I_gain = 1.7
refl_135_Q_gain = 1.25 #2.0


###########################
## DRMI POPAIR
###########################

prclPopair9I = 0.27
srclPopair45I = 0.74
michPopair45Q = 0.54
srclPopair9I = 0.04

###########################
## DRMI POP
###########################

prclPop9I = 0.035
srclPop45I = 0.08
michPop45Q = 0.08
srclPop9I = 0.005

###########################
## prmi sideband
###########################

# for REFL_RF45_I
prmisb_prcl_gain = 11 # 22 #5.25 #5 #when build is 160 in RF18, gain should be more like 2.5

# for REFL_RF9_I
#prmisb_prcl_gain = 2.1

prmisb_prcl_trig_upper_threshold = 3
prmisb_prcl_trig_lower_threshold = 1

prmisb_prcl_trig_fm_upper_threshold = 3.5
prmisb_prcl_trig_fm_lower_threshold = 1.0

prmisb_mich_gain = 15 #27.5 #12.4   # was 4 before RF amp was removed from 45

prmisb_mich_trig_upper_threshold = 4.0
prmisb_mich_trig_lower_threshold = 0.8

prmisb_mich_trig_fm_upper_threshold = 4.5
prmisb_mich_trig_fm_lower_threshold = 1.0

prmisb_locked_threshold = 25  # SPOP_18

###########################
## prmi car
###########################
prmicar_prcl_gain = -11
prmicar_mich_gain = -50

prmicar_prcl_trig_upper_threshold = -1
prmicar_prcl_trig_lower_threshold = -1

prmicar_prcl_trig_fm_upper_threshold = 2
prmicar_prcl_trig_fm_lower_threshold = 1

prmicar_mich_trig_upper_threshold = 4.0
prmicar_mich_trig_lower_threshold = 1.0

prmicar_mich_trig_fm_upper_threshold = 4.5
prmicar_mich_trig_fm_lower_threshold = 1.0

prmicar_locked_threshold = 2000 # POP DC

prmi_car_asc_gains = {
    'ASC-MICH_P_GAIN':   -0.8,
    'ASC-MICH_Y_GAIN':   -0.5,
    'ASC-PRC1_P_GAIN':   10.0,
    'ASC-PRC1_Y_GAIN':  -10.0,
    'ASC-PRC2_P_GAIN':   80.0,
    'ASC-PRC2_Y_GAIN': -300.0}

prmi_car_asc_outputs = {
    'ASC-OUTMATRIX_P_1_3': 1,
    'ASC-OUTMATRIX_Y_1_3': 1,
    'ASC-OUTMATRIX_P_3_4': 1,
    'ASC-OUTMATRIX_Y_3_4': 1,
    'ASC-OUTMATRIX_P_4_5': 1,
    'ASC-OUTMATRIX_Y_4_5': 1}

###########################
## mich 
###########################

# RFeb,9,2015, increased by a factor of 4 to compensate the low whitening gain
#michdark_gain_acq = -200
#michdark_gain = -500
michdark_gain_acq = -800
michdark_gain = -2000
michdark_locked_threshold = 130 #30

michbright_gain_acq = 500
michbright_gain = 500
michbright_locked_threshold = 7 

###########################
## prxy
###########################
prxy_gain = -3200 #-80 sheila increased sept 18, I'm not sure why I had to increase this to get 20 Hz ugf
prxy_asair_lock_threshold = 200
prxy_asair_min_fringe = 1500
prxy_popairdc_oscillating_thresh = 6
prxy_REFL9_whiten_gain = 0 # REFL_A_RF9

###########################
## srxy
###########################
srxy_gain = -10000 #used to be -10000
sry_locked_threshold = 100

###########################
## arm IR
###########################
arm_IR_gain = dict()
arm_IR_gain['X'] = 0.02 # 0.03 worked for x but not y, 0.03 caused oscialltions on december 13
arm_IR_gain['Y'] = 0.02
arm_IR_acquire_FMs = ['FM3', 'FM4', 'FM5']
arm_IR_trig_on = 0.2
arm_IR_trig_off = 0.05
arm_IR_FM_trig_on = 0.45
arm_IR_FM_trig_off = 0.2
arm_ASAIR_whiten_gain = 6 # corresponding to a whitening gain of 18 dB
